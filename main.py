from src.common.const import SUCCESS_CODE
from src.service.issue import CIssueBody, Pagination, Sort, UpdateParam, FormData, InsertParam
from src.service.project import CProjectBody, FieldData
from src.service.utils import LoginUtil, CreateProjectUtil, GetIssueListUtil, GetIssueDetailUtil, UpdateIssueUtil, \
    GetCreateFormUtil, CreateIssueUtil, GetFieldListUtil, GetFieldOptionUtil


# 测试创建工作项目
def createProject(user):
    f1 = FieldData()
    f1.field_id = "ed-22"
    f1.field_key = "project_name"
    f1.field_value = "py_test4"

    f2 = FieldData()
    f2.field_id = "ed-23"
    f2.field_key = "pkey"
    f2.field_value = "PTC"
    flist = []
    flist.append(f1)
    flist.append(f2)

    cbody = CProjectBody()
    cbody.issue_num = "1"
    cbody.type_id = "pt-4"
    cbody.type_name = "缺陷型项目"
    cbody.tenant_id = "6"
    cbody.field_data = flist
    response = CreateProjectUtil(user["data"]["token"], "6", cbody)
    print(response)


def getIssueList(user):
    body = CIssueBody()
    body.project_id = "PJBUG2022042246f8c"
    body.tenant_id = "6"
    body.search = ""
    sort = Sort()
    sort.field_key = "created_at"
    sort.is_desc = True
    pagination = Pagination()
    pagination.index = 1
    pagination.number = 20
    body.pagination = pagination
    body.sort = sort
    response = GetIssueListUtil(user["data"]["token"], "6", body)
    print(response)


def getIssueDetail(user):
    response = GetIssueDetailUtil(user["data"]["token"], "6", "PTB-1")
    print(response)


def createIssue(user):
    body = InsertParam()
    body.project_id = "PJBUG2022042246f8c"
    body.tenant_id = "6"

    fd1 = FormData()
    fd1.field_id = "fd-4"
    fd1.field_key = "project"
    fd1.field_value = "PJBUG2022042246f8c"
    body.form_data.append(fd1)

    fd2 = FormData()
    fd2.field_id = "fd-1"
    fd2.field_key = "issue_type"
    fd2.field_value = "it-1"
    body.form_data.append(fd2)

    fd3 = FormData()
    fd3.field_id = "fd-6"
    fd3.field_key = "summary"
    fd3.field_value = "test111"
    body.form_data.append(fd3)

    fd4 = FormData()
    fd4.field_id = "fd-5"
    fd4.field_key = "priority"
    fd4.field_value = "3"
    body.form_data.append(fd4)

    fd5 = FormData()
    fd5.field_id = "fd-11"
    fd5.field_key = "reporter"
    fd5.field_value = "ab4f6fa8-64bc-3a57-cdfc-f6bc09bcb506"
    body.form_data.append(fd5)

    fd6 = FormData()
    fd6.field_id = "fd-8"
    fd6.field_key = "issue_id"
    fd6.field_value = ""

    response = CreateIssueUtil(user["data"]["token"], "6", body)
    print(response)


def updateIssueUtil(user):
    form_data = FormData()
    form_data.field_id = "fd-6"
    form_data.field_key = "summary"
    form_data.field_value = "test"
    body = UpdateParam()
    body.issue_id = "PTB-1"
    body.tenant_id = "6"
    body.form_data.append(form_data)
    response = UpdateIssueUtil(user["data"]["token"], "6", body)
    print(response)


def getCreateFormUtil(user):
    response = GetCreateFormUtil(user["data"]["token"], "PJBUG20220422c9a77", "6")
    print(response)


def getFieldListUtil(user):
    response = GetFieldListUtil(user["data"]["token"], "6")
    print(response)


def getFieldOptionUtil(user):
    response = GetFieldOptionUtil(user["data"]["token"], "PJBUG2022042246f8c", "6", "issue_type")
    print(response)


if __name__ == '__main__':
    user = LoginUtil("17600601024", "123456")
    if user["code"] != SUCCESS_CODE:
        print(user)
        exit(0)
    getFieldOptionUtil(user)
