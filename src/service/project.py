import json

from json import JSONEncoder
import requests

from src.common import const
from src.utils.request_util import PostUtil


class FieldData(object):
    field_id: str
    field_key: str
    field_value: str

    def toJson(self):
        return {
            "field_id": self.field_id,
            "field_key": self.field_key,
            "field_value": self.field_value,
        }


# 创建工作项参数
class CProjectBody(object):
    issue_num: str
    type_id: str
    type_name: str
    tenant_id: str
    field_data: []

    def toJson(self):
        j = {
            "issue_num": self.issue_num,
            "type_id": self.type_id,
            "type_name": self.type_name,
            "tenant_id": self.tenant_id,
        }
        l = []
        for i in range(0, len(self.field_data)):
            item = self.field_data[i].toJson()
            l.append(item)
        j["field_data"] = l
        return j


# 创建项目 body-CProjectBody
def CreateProject(token, tenant_id, body):
    return PostUtil(const.BASE_URL + const.CREATE_PROJECT, token, tenant_id, None, body.toJson())
