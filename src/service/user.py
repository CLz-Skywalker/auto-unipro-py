# -*- coding:utf-8 -*-
# 用户操作
import json
import requests

from src.common import const
from src.common.const import RESPONSE_CODE, RESPONSE_MSG, SUCCESS_CODE


def dictToRole(d):
    return Role(d["id"], d["name"], d["is_bool"])


# 角色
class Role:
    id: int
    name: str
    isBool: bool

    def __init__(self, id, name, isBool):
        self.id = id
        self.name = name
        self.isBool = isBool


def dectToUserInfo(d):
    roles = []
    for i in range(0, len(d["roles"])):
        r = dictToRole(d["roles"][i])
        roles.append(r)
    return UserInfo(d["id"], d["name"], d["phone"], d["mail"], d["tenant_id"], d["avatar"], d["title"], roles)


# 用户信息
class UserInfo:
    id: int
    name: str
    phone: str
    mail: str
    tenant_id: int
    avatar: str
    title: str
    roles: [Role]

    def __init__(self, id, name, phone, mail, tenant_id, avatar, title, roles):
        self.id = id
        self.name = name
        self.phone = phone
        self.mail = mail
        self.tenant_id = tenant_id
        self.avatar = avatar
        self.title = title
        self.roles = roles


def dictToUserResponse(d):
    return UserResponse(d["token"], d["expire"], d["user"])


# 登陆成功响应结果
class UserResponse:
    token: str
    expire: str
    user: UserInfo

    def __init__(self, token, expire, user):
        self.token = token
        self.expire = expire
        u = dectToUserInfo(user)
        self.user = u

    # 登录
    def login(self, phone, password):
        data = {
            "phone": phone,
            "password": password
        }
        request_json = json.dumps(data)
        response = requests.post(url=const.LOGIN_URL + const.LOGIN, data=request_json, )
        if response.json()[RESPONSE_CODE] != SUCCESS_CODE:
            return response
        response_json = response.json()["data"]
        user = dictToUserResponse(response_json)
        params = {
            "authToken": user.token
        }
        response = requests.get(url=const.BASE_URL + const.EXCHANGE_TOKEN, params=params)  # user.token
        return response

