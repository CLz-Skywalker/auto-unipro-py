from src.common.const import SUCCESS_CODE
from src.service.issue import GetIssueList, GetIssueDetail, UpdateIssue, GetCreateForm, GetFieldList, GetFieldOption, \
    CreateIssue
from src.service.project import CreateProject
from src.service.user import UserResponse


# 登录
def LoginUtil(phone, password):
    user = UserResponse
    response = user.login(user, phone, password)
    return response.json()


# 创建工作项,body->CProjectBody
def CreateProjectUtil(token, tenant_id, body):
    response = CreateProject(token=token, tenant_id=tenant_id, body=body)
    return response.json()


# 获取工作项列表,body->CIssueBody
def GetIssueListUtil(token, tenant_id, body):
    response = GetIssueList(token=token, tenant_id=tenant_id, body=body)
    return response.json()


# 获取工作项详情
def GetIssueDetailUtil(token, tenant_id, issue_id):
    response = GetIssueDetail(token, tenant_id, issue_id)
    return response.json()


# 编辑工作项 issue,body->InsertParam
def CreateIssueUtil(token, tenant_id, body):
    response = CreateIssue(token, tenant_id, body)
    return response.json()


# 编辑工作项 issue,body->UpdateParam
def UpdateIssueUtil(token, tenant_id, body):
    response = UpdateIssue(token, tenant_id, body)
    return response.json()


# 获取创建工作项的字段
def GetCreateFormUtil(token, project_id, tenant_id):
    response = GetCreateForm(token, project_id, tenant_id)
    return response.json()


# 获取筛选分类
def GetFieldListUtil(token, tenant_id):
    response = GetFieldList(token, tenant_id)
    return response.json()


# 获取筛选分类条件
def GetFieldOptionUtil(token, project_id, tenant_id, field_key):
    response = GetFieldOption(token, project_id, tenant_id, field_key)
    return response.json()
