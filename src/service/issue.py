import json

import requests

from src.common import const
from src.utils.request_util import PostUtil, GetUtil


class Pagination(object):
    index: int
    number: int

    def __init__(self):
        self.index = 0
        self.number = 0

    def toJson(self):
        return {
            "index": self.index,
            "number": self.number,
        }


class FilterItem(object):
    field_key: str
    field_name: str
    field_type: str
    operator_key: str
    operator_name: str
    value: []
    value_name: []

    def __init__(self):
        self.field_key = ""
        self.field_name = ""
        self.field_type = ""
        self.operator_key = ""
        self.operator_name = ""
        self.value = []
        self.value_name = []

    def toJson(self):
        return {
            "field_key": self.field_key,
            "field_name": self.field_name,
            "field_type": self.field_type,
            "operator_key": self.operator_key,
            "operator_name": self.operator_name,
            "value": self.value,
            "value_name": self.value_name,
        }


class Sort(object):
    field_key: str
    is_desc: bool

    def __init__(self):
        self.field_key = ""
        self.is_desc = False

    def toJson(self):
        return {
            "field_key": self.field_key,
            "is_desc": self.is_desc,
        }


class CIssueBody(object):
    tenant_id: str
    search: str
    project_id: str
    sort: Sort()
    pagination: Pagination()
    filter: []

    def __init__(self):
        self.tenant_id = ""
        self.search = ""
        self.project_id = ""
        self.sort = Sort()
        self.pagination = Pagination()
        self.filter = []

    def toJson(self):
        j = {
            "tenant_id": self.tenant_id,
            "search": self.search,
            "project_id": self.project_id,
            "sort": self.sort.toJson(),
            "pagination": self.pagination.toJson(),
        }
        l = []
        for i in range(0, len(self.filter)):
            item = self.filter[i].toJson(self.filter[i])
            l.append(item)
        j["filter"] = l
        return j


class FormData(object):
    field_id: str
    field_value: str
    field_key: str

    def __init__(self):
        self.field_id = ""
        self.field_value = ""
        self.field_key = ""

    def toJson(self):
        return {
            "field_id": self.field_id,
            "field_value": self.field_value,
            "field_key": self.field_key,
        }


class IssueAttachment(object):
    field_id: str
    name: str
    size: str
    link_url: str
    content: str

    def __init__(self):
        self.field_id = ""
        self.name = ""
        self.size = ""
        self.link_url = ""
        self.content = ""

    def toJson(self):
        return {
            "field_id": self.field_id,
            "name": self.name,
            "size": self.size,
            "link_url": self.link_url,
            "content": self.content,
        }


class IssueLink(object):
    link_type_id: str
    destination_issue_id: str
    field_id: str

    def __init__(self):
        self.link_type_id = ""
        self.destination_issue_id = ""
        self.field_id = ""

    def toJson(self):
        return {
            "link_type_id": self.link_type_id,
            "destination_issue_id": self.destination_issue_id,
            "field_id": self.field_id,
        }


class InsertParam(object):
    project_id: str
    tenant_id: str
    form_data: [FormData]
    issue_attachment: [IssueAttachment]
    issue_link: [IssueLink]

    def __init__(self):
        self.project_id = ""
        self.tenant_id = ""
        self.form_data = []
        self.issue_attachment = []
        self.issue_link = []

    def toJson(self):
        r = {
            "project_id": self.project_id,
            "tenant_id": self.tenant_id,
        }
        fd = []
        for i in range(0, len(self.form_data)):
            item = self.form_data[i].toJson()
            fd.append(item)
        r["form_data"] = fd
        fd = []
        for i in range(0, len(self.issue_attachment)):
            item = self.issue_attachment[i].toJson()
            fd.append(item)
        r["issue_attachment"] = fd
        fd = []
        for i in range(0, len(self.issue_link)):
            item = self.issue_link[i].toJson()
            fd.append(item)
        r["issue_link"] = fd
        return r

class UpdateParam(object):
    tenant_id: str
    issue_id: str
    form_data: [FormData]

    def __init__(self):
        self.tenant_id = ""
        self.issue_id = ""
        self.form_data = []

    def toJson(self):
        r = {
            "tenant_id": self.tenant_id,
            "issue_id": self.issue_id,
            "form_data": self.form_data,
        }
        l = []
        for i in range(0, len(self.form_data)):
            item = self.form_data[i].toJson()
            l.append(item)
        r["form_data"] = l
        return r


# 获取工作项列表
def GetIssueList(token, tenant_id, body):
    return PostUtil(const.BASE_URL + const.ISSUE_LIST, token, tenant_id, None, body.toJson())


# 获取工作项详情
def GetIssueDetail(token, tenant_id, issue_id):
    params = {
        "tenant_id": tenant_id,
        "issue_id": issue_id
    }
    return GetUtil(const.BASE_URL + const.ISSUES_DETAIL, token, tenant_id, params)


def CreateIssue(token, tenant_id, body):
    return PostUtil(const.BASE_URL + const.CREATE_ISSUE, token, tenant_id, None, body.toJson())


# 编辑工作项 issue
def UpdateIssue(token, tenant_id, body):
    return PostUtil(const.BASE_URL + const.MODIFY_PROJECT, token, tenant_id, None, body.toJson())


# 获取创建工作项的字段
def GetCreateForm(token, project_id, tenant_id):
    params = {
        "tenant_id": tenant_id,
        "project_id": project_id
    }
    return GetUtil(const.BASE_URL + const.GET_FORM, token, tenant_id, params)


# 获取筛选分类
def GetFieldList(token, tenant_id):
    return GetUtil(const.BASE_URL + const.GET_FILTER, token, tenant_id, None)


# 获取筛选分类条件
def GetFieldOption(token, project_id, tenant_id, field_key):
    data = {
        "tenant_id": tenant_id,
        "project_id": project_id,
        "field_key": field_key
    }
    return PostUtil(const.BASE_URL + const.GET_FILTER_OPTION, token, tenant_id, None, data)
