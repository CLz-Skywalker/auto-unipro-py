BASE_URL = "http://project-dev.innovsharing.com/api/backend"
LOGIN_URL = "http://low-code-dev.innovsharing.com/cy2/api/v1"
# "http://8.131.241.132:3240/api/v1"

LOGIN = "/login"  # 登录
EXCHANGE_TOKEN = "/exchangeToken"  # 交换token
CREATE_PROJECT = "/projects/v1/create"  # 创造项目
CREATE_ISSUE="/issues/v1/create"
MODIFY_PROJECT = "/issues/v1/modify_info"  # 编辑工作项
ISSUE_LIST = "/projects/v1/issuelist"  # 获取工作项
ISSUES_DETAIL = "/issues/v1/detail"  # 获取工作项详情
GET_FORM = "/issues/v1/create_form"  # 获取创建工作项的字段
GET_FILTER = "/projects/v1/filter/fields"  # 获取筛选分类
GET_FILTER_OPTION = "/projects/v1/issuelist/fieldOption"  # 获取具体筛选条件

RESPONSE_CODE = "code"
RESPONSE_MSG = "message"
RESPONSE_DATA = "data"

SUCCESS_CODE = 0
