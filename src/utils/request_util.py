import json

import requests


def PostUtil(url, token, tenant_id, params, body):
    request_json = json.dumps(body, ensure_ascii=False).encode("utf-8")
    header = {
        "Content-Type": "application/json",
        "cyToken": token,
        "tenantId": tenant_id
    }
    if params is None:
        params = {}
    response = requests.post(url=url, headers=header, params=params, data=request_json)
    return response


def GetUtil(url, token, tenant_id, param):
    header = {
        "Content-Type": "application/json",
        "cyToken": token,
        "tenantId": tenant_id
    }
    if param is None:
        param = {}
    response = requests.get(url=url, headers=header, params=param)
    return response
